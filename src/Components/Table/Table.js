function Table(){
    return (
        <table>
            <thead>
                <tr>
                    <th>Знак</th>
                    <th>Дата</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Козерог</td>
                    <td>23 декабря — 20 января</td>
                </tr>
                <tr>
                    <td>Водолей</td>
                    <td>21 января — 19 февраля</td>
                </tr>
                <tr>
                    <td>Рыбы</td>
                    <td>20 февраля — 20 марта</td>
                </tr>
                <tr>
                    <td>Овен</td>
                    <td>21 марта — 20 апреля</td>
                </tr>
                <tr>
                    <td>Телец</td>
                    <td>21 апреля — 21 мая</td>
                </tr>
                <tr>
                    <td>Близнецы</td>
                    <td>22 мая — 21 июня</td>
                </tr>
                <tr>
                    <td>Рак</td>
                    <td>22 июня — 22 июля</td>
                </tr>
                <tr>
                    <td>Лев</td>
                    <td>23 июля — 21 августа</td>
                </tr>
                <tr>
                    <td>Дева</td>
                    <td>22 августа — 23 сентября</td>
                </tr>
                <tr>
                    <td>Весы</td>
                    <td>24 сентября — 23 октября</td>
                </tr>
                <tr>
                    <td>Скорпион</td>
                    <td>24 октября — 22 ноября</td>
                </tr>
                <tr>
                    <td>Стрелец</td>
                    <td>23 ноября — 22 декабря</td>
                </tr>
            </tbody>
        </table>
    );
}

export default Table;